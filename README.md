# Evaluating Static Site Generator (SSG) for Product Documentation

## About
This is a compilation of quick research on SSGs that exist today in the market and an evaluation based on the reserach.

The purpose of this article is to compare different SSG tools from the perspective of using them for product documentation.

## Background
If you are a member of a product development team, and you care about the users - your customer, you will probably want to have a site that that includes documentation about the product.
A well structured and written documentation can accelerate the adoption of your product and save you time by avoiding fequent 1:1 intraction with the user.

If _you_ are a software developer, you probably do not enjoy writing documentation. Let's be honest, it is no fun. But then again, how would you have learned the technlology that makes you happy if it weren't for its documentation? So it is your turn to create documentation of software that you are building.

In recent years, technologies such as plain-text [lightweight markup languages](https://en.wikipedia.org/wiki/Lightweight_markup_language), e.g. [Markdown]() and document oriented Static Site Generators helped mitigate the painful developer experience of creating technical documentaiton. NOTE: the tool will not alliviate the burdern related to your writing skills!.

Too much for the Intro. Let's jump to the list of tools evaluated.

## But Wait! Why Not Just Wiki or CMS?
It is a great question to start. Here are some reasons to use SSG 
- Simple, no run-time dependency (just http)
- Version Controlled - Keep it close to the code
- Can Leverage (free) Static Hosting - such as Netlify, GitLab pages, Github pages
- Extremely Fast Access
- Scaleable
- Very Secure - no security patch headaches

I think each bullet is self-explanatory, but if you want further details read [this aricle](https://learn.cloudcannon.com/jekyll/why-use-a-static-site-generator/) which I referened.

Particularly for maintaining a software documentation the main convincing factor is that the SSG tool help us achieve [Docs-as-Code](https://www.writethedocs.org/guide/docs-as-code/) philosophy.

By keeping the documentation as code, you can keep it in the repository where your code. Think about the implications: 
- you'd document using the same tool as coding, 
- you can code-review, and 
- include publishing as part of the deploy pipeline

## Tool Candidates
The candidates are not the exhaustive list of tools, but a selection based of reviews of articles I found in the internet and popularity (number of stars) in Github (see [staticgen.com](https://www.staticgen.com)).

Don't be too sad if your favorite SSG tool is not mentioned here.

| Tool Name        | Implementation       | Highlight                                                    | Formats / Template   | Limitations                               | Who's Using                      | Sample Project |
|------------------|----------------------|--------------------------------------------------------------|----------------------|-------------------------------------------|----------------------------------|----------------|
| [Antora](https://antora.org/)           | node                 | Maintained by lead contributors to the Asciidoctor project\. | Md, Ad               | Small community                           | Fedora                           |                |
| [Docsify](https://docsify.js.org/)          | node                 | Actually this isn't SSG                                      | Md / Vue             | Needs server                              | pino                             |                |
| [Docusaurus](https://docusaurus.io/)       | node, React          | Maintained by Facebook\. Very active community               | Md / React           |                                           | Babel, Jest                      | GitLab         |
| [Gatsby](https://www.gatsbyjs.org/)           | node, React, GraphQL | Very Popular, backed by company of same name\.               | Md / React           | Generic tool\. Not document\-focused tool | React, Apollo                    | GitLab         |
| [GitBook cli](https://github.com/GitbookIO/gitbook-cli)          | node                 | Very popular                                                 | Md                   | cli development halted in favor of SaaS   | gitbook\.com                     | GitLab         |
| [Hexo](https://hexo.io/)             | node                 | Seems to be popular, but mostly in China                     | Md / EJS             | resources mostly in Chinese               |                                  | GitLab         |
| [Hugo](https://gohugo.io/)             | go                   | Popular, specially among go projects                         | Md, Ad / Go Template | Template learning curve                   | smashingmagazine, docker, linode | GitLab         |
| [Jekill](https://jekyllrb.com/)           | Ruby                 | Engine for GitHub pages                                      | Md                   | Slow on large project                     |                                  |                |
| [mdBook](https://github.com/rust-lang/mdBook)           | rust                 | Drop\-in replacement of GitBook                              | Md                   |                                           |                                  |                |
| [mkdocs](https://www.mkdocs.org/)           | python               | Powers read\-the\-docs                                       | Md / Jinja           | Fewer features                            |                                  | GitLab         |
| [Spring REST docs](https://spring.io/projects/spring-restdocs) | Java, Spring         | Tight integration with Spring testing                        | Ad, Md / Mustache    | API\-focused documentation                |                                  |                |
| [VuePress](https://vuepress.vuejs.org/)         | node, Vue            | Large community                                              | Md / Vuew            | Generic tool\. Not document\-focused tool |                                  | GitLab         |


## Evaluation
I took hands-on approach for the evaluation. I did not prepared an extensive feature matrix with checks, but rather captured my first-impression on following there "Getting-Started" documentation.

### Evaluation Criteria
When testing the tools I was mostly focused on:
- *Ecosystem:* Are ther plenty documentation, numerous plugins and themes?
- *Seamless of installation and deployment (publishing):* How easy is to install the tool, and plugins/themes? How much effort is it required to produce a decent documentation site (E.g. intuitive layout/look and feel)? And how easy is to deploy to GitLab pages - my primary deployment environment? 
- *Writer Experience:* How quick can I learn the markup language and template? Does it have Link Checker? Does it produce TOC? Hot reload?
- *User (Reader) Experience (Features):* Nav bar? Color Theming? Search? Code highlight? Breadcrumb? Integration with MermaidJS? 
- *Community & Popularity:* The community is active. (This one could be subjective) Lots of stars in project =)

### Evaluation Method
Basically the method was to install and serve and add two pages that includes codes (Java and shell), embedded image and a simple MermaidJS diagram.

### Evaluation Result
[TBD]

## References
- [Why use a static site generator](https://learn.cloudcannon.com/jekyll/why-use-a-static-site-generator/)

Other Links
- https://gist.github.com/briandominick/e5754cc8438dd9503d936ef65fffbb2d
- https://github.com/getzola/zola
- https://fedoramagazine.org/using-antora-for-your-open-source-documentation/