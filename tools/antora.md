# Antora

> The multi-repository documentation site generator for tech writers who  writing in AsciiDoc.

## Basics
- Site: https://antora.org/
- Repo: https://gitlab.com/antora/antora
- Language: JavaScript
- Tested Version: 2.2 ()
- First Release: V1.0.0 - 01/04/2018

## Ecosystem

## Installation
Following instruction at the [tool's documentation](https://docs.antora.org/antora/2.2/install/install-antora/):
```sh
$ npm i -g @antora/cli@2.2 @antora/site-generator-default@2.2
```
As name implies, the first package is the CLI and the second is the site generator.

Then the concept of playbook and document component is a little confusing in the beginning.  You have to keep in mind that this tool is a `multi-repository documentation site generator`.

To pull documentation from multip repo, Antora requires an instruction and pointers to those repositoires. That's the purpose of [`antora-playbook.yml`](https://docs.antora.org/antora/2.2/playbook/) file.

Once the playbook file is crated, to generate the files run the following command:
```
$ antora antora-playbook.yml
```

## Deployment

## Writing Experience

## Reading Experience

## Overall Impression
