# Docusaurus

## Basics
- Site: https://docusaurus.io
- Repo: 
- Language: 
- Tested Version: 
- First Release: 

## Ecosystem

## Installation
Following the instructions in the [document](https://docusaurus.io/docs/en/installation).

### Step 1: Create a project folder
```sh
$ mkdir sample-docusaurus &&  cd "$_"
```

### Step 2: Run docusaurus-init
Then the documentation suggest to install `docussarus-init` globally using `yarn`,
```sh
$ yarn global add docusaurus-init
$ docusaurus-init
```

But I used `npx` instead
```sh
$ npx docusaurus-init
```

After 4.3 mins on my laptop, I noticed it generated
```
(file) .dockerignore
(file) .gitignore
(file) Dockerfile
(file) docker-compose.yml
(folder) docs
(folder) website 
```

### Step 3: Run the server (locally)
Following the instruction on their Getting Started,
```
$ cd website
$ yarn start
```
It opened up my browser to the local address `http://localhost:3000`, showing and default skeleton site with olive-colored theme.


## Deployment
According to GitLab pages Docusaurus sample's [.gitlab-ci.yml](https://gitlab.com/pages/docusaurus/blob/master/.gitlab-ci.yml) file

```yaml
image: node:9.11.1

pages:
  script:
  - cd website
  - yarn install
  - yarn build
  # The build directory is created based on the value set for projectName in
  # website/siteConfig.js. If you change it there, you need to change it here
  # as well.
  - mv ./build/docusaurus ../public

  artifacts:
    paths:
    - public

  only:
  - master
```

## Customizing Layout/Theme for Document Site
The footer is under website/core/Footer.js which contains a sinle `Footer` React class.

Configuration `siteConfig.js` includes parameters for:
- Site Name, tagline, baseUrl, copyright
- Top-navigation bar menu (`headerLinks`)
- Icons: header, footer, favicon
- Colors: Primary and secondary (a la Bootstrap)
- Fonts

### Side menu
The `sidebars.json` allows to customoize side menu in arbitrary order and hierarchy.
```json
{
  "docs": {
    "Docs": ["doc1", "doc2"],
    "Second Category": ["doc3"]
  },
  "docs-other": {
    "First Category": ["doc4", "doc5"]
  }
}
```
But changing the `sidebars.json` does not trigger reload.

*NOTE*: 

*NOTE*: I have not found what the `other-docs` is for.

## Writing Experience
The directory structure is explained in the [documentation](https://docusaurus.io/docs/en/site-preparation).

The Docusaurs alllows you to 
1. Customize website - by modifying files under `/website` directory you can customize the look of the landingpage and footer
2. Add documentation pages - by adding markdown files in `/docs` directory you can add documentation pages
3. Add blogs - by adding markdown files in `/website/blog` you can add blog entries

### Creating a new documentation page
To create a new page, just add a new markdown file in `/docs` directory.
The page should have a proper front matter (metadata) header section
```md
---
id: intro
title: Getting Started
---
My new content here..
```

The `$yarn start` provides hot-reload, even config (`siteConfig.js`) changes are reflected immediately, but unfortunately changes on the `sidebars.json` are not picked up.

### Integrating with MermaidJS
Embedding the div just works:
```html
<div class="mermaid">
graph LR;
  A-->B;
</div>
<script async src="https://unpkg.com/mermaid@8.2.3/dist/mermaid.min.js"></script>
```

### Customizing the Landing Page
From [tool's document](https://docusaurus.io/docs/en/site-creation#docs-landing-page)


## Reading Experience
In general, the site generated follows the documentation convention.
Top bar that contains site navitagin, and right-side-bar that contains document or blog navigation.

The document automatically generates "Prev" and "Next" links.


## Overall Impression

Nothing extraordinary that catches the eyes.
