# Hugo

## Basics
- Site: https://www.gatsbyjs.org
- Repo: 
- Language: 
- Tested Version: 
- First Release: 

## Ecosystem

## Installation
Following the [tool's tutorial](https://www.gatsbyjs.org/tutorial/part-zero/)

```
$ npm install -g gatsby-cli.
```

## Installing the gatsby-theme-apollo-docs Theme

$ gatsby new sample-gatsby-apollo

Then I follow the instructions at [gatsby-theme-apollo-docs](https://github.com/apollographql/gatsby-theme-apollo/tree/master/packages/gatsby-theme-apollo-docs)

$ npm install gatsby react react-dom
$ npm install gatsby-theme-apollo-docs
$ gatsby develop

FAILS WITH
```
There was an error in your GraphQL query:

Cannot query field "siteMetadata" on type "Site".
...
```

--- Now trying
$ gatsby new sample-gatsby-apollo https://github.com/apollographql/gatsby-theme-apollo

FAILURE

## Instlling the codebushi/gatsby-theme-document Theme
```
$ gatsby new sample-gatsby-document https://github.com/codebushi/gatsby-theme-document-example
$ cd sample-gatsby-document
$ gatsby develop
```
SUCCEED

## Deployment

## Writing Experience

## Reading Experience

## Overall Impression
