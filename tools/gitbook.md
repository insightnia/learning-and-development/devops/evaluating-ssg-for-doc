# Hugo

## Basics
- Site: https://github.com/GitbookIO/gitbook-cli
- Repo: 
- Language: 
- Tested Version: 
- First Release: 

## Ecosystem
Most of the documentation are for gitbook.com the site.
One documentation introduction I found useful is [this one](https://developpaper.com/gitbook-introductory-tutorial-using-gitbook-cli-to-develop-e-books/).


## Installation and Creating New Project
Assuming you have node installed, the installation of the tool is straigh forward.
```sh
$ sudo npm install -g gitbook-cli
```

To create a new project:
```sh
$ gitbook init
```
Which basically creates files README.md and SUMMARY.md.

## Deployment
To build
```
$ gitbook build
```
Which generates the static web page under `_book` directory.

## Customizing Layout/Theme for Documentation Site
You can create a `book.json` to to configure the site with following parameters:

- title
- author
- description
- isbn
- language
- direction - ("ltr" for left to right or "rtl" for right to left)
- root - the root directory
- links - additional links in the left navigation bar
- styles - custom styles
- plugins
And [few others](https://gitbookio.gitbooks.io/documentation/format/configuration.html)


## Authoring Experience

Authoring is simple consisting of creating markdown files and creating a Table of Content (TOC) in the `SUMMARY.md` also in Markdown format.


### Integration with MermaidJS 
Embedding the div just works:

```html
<div class="mermaid">
graph LR;
  A-->B;
</div>
<script async src="https://unpkg.com/mermaid@8.2.3/dist/mermaid.min.js"></script>
```
But there seems to be some timing issue when the page is loaded and script being applied.
Oftenimes I have to reload the page to get the diagram rendered.

## Reading Experience
*Search with local index comes out of the box*

## Overall Impression
Very simple operate and author.
The `SUMMARY.md` convention makes it easy to view in any git hosting site without the need of deployment.

