# mdBook

## Basics
- Site: https://github.com/rust-lang/mdBook
- Repo: 
- Language: 
- Tested Version: 
- First Release: 

## Ecosystem

## Installation
Following the [tool's document](https://github.com/rust-lang/mdBook).

Given that rust is already installed, the collowing command will instal mdbook:
```sh
$ cargo install mdbook
```
The alternative is to just download the [binary](https://github.com/rust-lang/mdBook/releases) and have it reachable by `PATH`.

## Creating a New Project
```
$ mdbook init
```

This will create a diretory with the minimal boilerplate
```
root/
├── book
├── book.toml
└── src
    ├── chapter_1.md
    └── SUMMARY.md
```

Then to serve
```
$ mdbook serve
```

## Build & Deployment

```
$ mdbook build
```

## Authoring Experience
Same as gitbook.

## Reading Experience
Same as gitbook, but without search (OOTB)

## Overall Impression
