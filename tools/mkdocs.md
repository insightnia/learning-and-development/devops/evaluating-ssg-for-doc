# MkDocs

## Basics
- Site: https://www.mkdocs.org
- Repo: 
- Language: 
- Tested Version: 
- First Release: 

## Ecosystem

## Installation, New Project
Assuming you have python and pip, the installation of the tool is just 
```sh
$ pip install mkdocs
```
For further details see [tool's page](https://www.mkdocs.org/#installation).

To create a new project
```sh
$ mkdocs new my-project
```

The above command creates the following:
```
mkdocs.yml
+docs
|-index.md
```


## Deployment


## Customizing Layout/Theme for Documentation Site
Changing the theme to one that comes OOTB, say `readthedocs` is super easy, just modify the mkdocs.yml to: 
```yml
theme: readthedocs
```
To change favicon and styling. See the [tool's guide](https://www.mkdocs.org/user-guide/styling-your-docs/).

It is also possible to customize the theme by overriding `custom_dir` directory and using Jinja-based template. But for this excersize, readthedocs satisfies the purpose.

### Modifying Side Navigation
Modify the `mkdocs.yml` file. Example
```yml
nav:
- Home: 'index.md'
- User Guide:
    - 'Writing your docs': 'writing-your-docs.md'
    - 'Styling your docs': 'styling-your-docs.md'
- About:
    - 'License': 'license.md'
```

## Authoring Experience

### Creating new page
Just add Markdown files under `/docs` directory.
The files can be under sub-directories.

### Integration with MermaidJS 
Embedding the div just works:

```html
<div class="mermaid">
graph LR;
  A-->B;
</div>
<script async src="https://unpkg.com/mermaid@8.2.3/dist/mermaid.min.js"></script>
```
But there seems to be some timing issue when the page is loaded and script being applied.
Oftenimes I have to reload the page to get the diagram rendered.

## Reading Experience
Same as [Read The Docs](https://readthedocs.org/).
*Search with local index comes out of the box*

## Overall Impression
